library(tidyverse)

# wrapper for commandline md5sum
md5sumCmd <- function(filename){
  cat("Calculating MD5 for",filename,"\n")
  sub("([a-f0-9]+) .*","\\1",system(paste("md5sum",filename),intern=T))
}

generateMD5sums <- function(filesCSVfile,outputMD5file){
  read_csv(filesCSVfile, col_types = cols()) %>%
    gather(key="key",value = RawDataFile, starts_with("RawDataFile")) %>%
    filter(!is.na(RawDataFile)) %>%
    mutate(fullpath=file.path(Directory,subdir,RawDataFile)) %>% 
    mutate(md5 = map(fullpath, md5sumCmd)) %>% 
    with( writeLines(paste(md5,RawDataFile),con = outputMD5file) )  
}
