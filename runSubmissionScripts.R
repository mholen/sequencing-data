library(tidyverse)
#
# Runs scripts that aid submission of raw read data to Annotare. 
# This includes:
# 1. generate file tables of fastq files
# 2. generate shell script that upload the files via FTP
# 3. generate MD5 sums needed for FTP upload (note: this could take some minutes)
#
# Note: the generated files will be stored in the same folder as the sample metadata file you give.
# Note: The MD5 checksums may take some time to complete. You can start uploading with the FTP upload 
#       script generated in step 2 while waiting for the MD5.
#
#

# Enter full path to your sample metadata table (*_samplemeta.xlsx)
inputXLfile <- ""

# Enter the FTP upload submission address from Annotare (Click the "FTP/Aspara Upload" button in  Annotare to get this)
# E.g. If annotare tells you to upload to "ftp-private.ebi.ac.uk/gibberish/" then:
# ftpServer = "ftp-private.ebi.ac.uk"
# submissionFolder = "gibberish"
ftpServer = ""
submissionFolder = ""



XLsuffix <- "_samplemeta.xlsx"

if(substring(inputXLfile, nchar(inputXLfile)-nchar(XLsuffix)+1) != XLsuffix){
  stop('inputXLfile should always end with "',XLsuffix,'"')
} else {
  filePrefix = substring(inputXLfile, 1,nchar(inputXLfile)-nchar(XLsuffix))
}

outputCSVfile <- paste0(filePrefix,"_samplefiles.csv")
outputMD5file <- paste0(filePrefix,"_samplefiles.md5")
outputFTPscript <- paste0(filePrefix,"_FTPupload.sh")


#### 1. Generate fastq files table ####
#

source("scripts/findFastqFiles.R")
findFastqFiles(inputXLfile,outputCSVfile)


#### 2. Generate FTP upload script ####
#

source("scripts/makeFTPscript.R")
makeFTPscript(filesCSVfile = outputCSVfile, outputFTPscript = outputFTPscript, 
              ftpServer = ftpServer, submissionFolder = submissionFolder)


#### 3. Generate MD5 checksums ####
#

source("scripts/generateMD5sums.R")
generateMD5sums(filesCSVfile = outputCSVfile, outputMD5file)

