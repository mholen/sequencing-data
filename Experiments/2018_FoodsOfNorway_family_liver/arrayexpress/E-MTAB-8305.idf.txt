MAGE-TAB Version	1.1
Investigation Title	RNA-seq of salmon liver from different families

Experimental Design
Experimental Design Term Source REF
Experimental Design Term Accession Number

Experimental Factor Name	individual
Experimental Factor Type	individual
Experimental Factor Term Source REF	EFO
Experimental Factor Term Accession Number	EFO_0000542

Person Last Name	Sandve
Person First Name	Simen Rød
Person Mid Initials
Person Email	simen.sandve@nmbu.no
Person Phone
Person Fax
Person Affiliation	Norwegian University of Life Sciences
Person Address	Arboretveien 6, 1430, Ås, Norway
Person Roles	data analyst;funder;institution;submitter
Person Roles Term Source REF
Person Roles Term Accession Number

Quality Control Type
Quality Control Term Source REF
Quality Control Term Accession Number

Replicate Type
Replicate Term Source REF
Replicate Term Accession Number

Normalization Type
Normalization Term Source REF
Normalization Term Accession Number

Date of Experiment	2017-10-01
Public Release Date	2019-10-18

PubMed ID
Publication DOI
Publication Author List
Publication Title
Publication Status
Publication Status Term Source REF
Publication Status Term Accession Number

Experiment Description	To analysis gene expression of lipid metabolism regulation between salmon families with different estimated breeding value of lipid content in muscle

Protocol Name	P-MTAB-89221	P-MTAB-89220	P-MTAB-89222	P-MTAB-89219
Protocol Type	nucleic acid library construction protocol	nucleic acid extraction protocol	nucleic acid sequencing protocol	sample collection protocol
Protocol Term Source REF	EFO	EFO	EFO	EFO
Protocol Term Accession Number	EFO_0004184	EFO_0002944	EFO_0004170	EFO_0005518
Protocol Description	Nucleic acid library was prepared by using TruSeq Stranded mRNA Library Prep Kit (Illumina, San Diego, CA, USA), follow the protocol.	RNA from liver tissue was extracted by using RNeasy Plus Universal Mini Kit (Cat No./ID: 73404), following the protocol. RNA concentration was measured by using NanoDrop 8000 (Thermo Scientific, Wilmington, USA) and Agilent 2100 Bioanalyzer (Agilent Technologies, Santa Clara, CA, USA).	The libraries were sequenced using 100bp single-end high-throughput mRNA sequencing (RNA-seq) on an Illumina Hiseq 4000 (Illumina, San Diego, CA, USA) amchine at the Norwegian Sequencing Centre (Oslo, Norway).	Experiment was done in fish lab of Norwegian University of Life Sciences. Fish was slaughtered when they were at ~20g in freshwater. Liver was flash frozen and stored in -80 before RNAseq.
Protocol Parameters
Protocol Hardware			Illumina HiSeq 4000
Protocol Software
Protocol Contact

Term Source Name	EFO	ArrayExpress
Term Source File	http://www.ebi.ac.uk/efo/	http://www.ebi.ac.uk/arrayexpress/
Term Source Version	2.38

SDRF File	E-MTAB-8305.sdrf.txt
Comment [TemplateType]	Animal - High-throughput sequencing
Comment [Submitted Name]	RNA-seq of salmon liver from different families
Comment [SecondaryAccession]	ERP117155
Comment [SequenceDataURI]	http://www.ebi.ac.uk/ena/data/view/ERR3508062-ERR3508797
Comment [AEExperimentType]	RNA-seq of coding RNA
Comment[ArrayExpressAccession]	E-MTAB-8305