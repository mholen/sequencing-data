# 1) read in fastq
# 2) pasre out second par
# 3) read in excel with samples from scotland
# 4) match fastq with sample info

# read in fastq-files from sequencing project

fastqsamples = read.table('~/Dropbox/Work/Sequencing/database_sequencing/Experiments/2015_David_Light&Salt_gill_exp1/fastqs_sequencing run 160121_7001448_0384_BC895PANXX - Project_Sandve-RNAlibs2-2015.txt', head=F)
fastqsplit = strsplit(as.character(fastqsamples$V1), '\\-')
sampleID  = gsub('_.*', '', sapply(fastqsplit, '[[', 2)) # get only sampleID info out
fastq_metainfo = data.frame(fastq=fastqsamples$V1, sampleID=sampleID, stringsAsFactors = F) # make a table with fastq and sampleID
fastq_metainfo <- fastq_metainfo[grep('^[a-zA-Z]', fastq_metainfo$sampleID, invert = T), ] 


# read in original sample data
sampledata = readxl::read_excel('~/Dropbox/Work/Sequencing/database_sequencing/Experiments/2015_David_Light&Salt_gill_exp1/Scotland Simen 2 plates TruSeq RNA 10nM libs delievered NSC 25th Nov 2015.xlsx', sheet = 1, skip = 9)
colnames(sampledata)[4] <- 'sampleID'
sampledata = sampledata[grep('^[a-zA-Z]', sampledata$sampleID, invert = T), ] # remove samples that are from other sequencing projects

#sanity check: both 168
max(as.numeric(sampledata$sampleID))
max(as.numeric(fastq_metainfo$sampleID))

# sanity checks..same numbers in sampleIDs
table(as.numeric(fastq_metainfo$sampleID) %in% unique(as.numeric(sampledata$sampleID)))
table(unique(as.numeric(sampledata$sampleID)) %in% as.numeric(fastq_metainfo$sampleID) )

#merge two tables
light_and_salt_sampleinfo = merge(fastq_metainfo, sampledata, by = 'sampleID')
light_and_salt_sampleinfo = light_and_salt_sampleinfo[order(as.numeric(light_and_salt_sampleinfo$sampleID), decreasing = F), ]

head(light_and_salt_sampleinfo); tail(light_and_salt_sampleinfo)

light_and_salt_sampleinfo$sampleID_groupname <- gsub(' ', '_', paste0(light_and_salt_sampleinfo$sampleID, '_', light_and_salt_sampleinfo$`Group name`))

write.csv(light_and_salt_sampleinfo, quote=F, file = '~/Dropbox/Work/Sequencing/database_sequencing/Experiments/2015_David_Light&Salt_gill_exp1/fastqIDs_and_sampleIDs.txt', row.names = F)
