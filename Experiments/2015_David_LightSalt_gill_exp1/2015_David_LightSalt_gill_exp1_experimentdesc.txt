# General information

## Title:

Photoperiodic control of smoltification

## Description:

This experiment is intended to provide information about transcriptional changes in the gill of Atlantic salmon during smoltification, in comparison with juvenile salmon not given the appropriate photoperiodic stimuli. Also, transcriptional changes due to osmotic stress (24H salt water challenged) will be recorded. This to better understand the transformation of the gill from a fresh water-type to a salt water-type, and possibly identify novel and important genes that are dependent upon a specific photoperiodic history for their expression. Additionally the data can be used to observe the shift in salt water response as the juvenile salmon goes through smoltification.

Experimental workflow:

All fish started on 24H light (LL), after one week, half of the fish were moved to short photoperiod (8:16, SP). The rest remained on LL throughout. After eight weeks, half of the fish on short photoperiod were moved back onto 24H light (SPLL). Remaining fish continued under 8:16. After another eight weeks the experiment was ended. Fish were sampled on days 0, 32, 53, 68, 89 and 110. On each sampling occasion fish were sampled directly from fresh water (FW), and from a 24H salt water (SW) stay, transferred the previous day. Weight and length was noted. Gill tissue was the taken and placed in RNAlater. Additionally plasma for measuring hypoosmoregulatory capacity was taken.

## Contact info:
david.hazlerigg@uit.no
m.iversen@uit.no

# Protocols
(Write a description for each of the protocols)

## (REQUIRED) sample collection protocol:

Fish were randomly netted out (n=6 for each treatment group), and placed in a 10 L container with anesthesia (Benzocaine, 150 ppm). Once lethally anesthetized length and weight of the fish was recorded. Plasma was drained from the caudal vein (2 ml Lithium-heparinized vacutainers, BD vacutainers®, Puls Norge, Moss, Norway). The fish was then decapitated, and tissue samples were taken. Gill tissue was stored in RNAlater for 24H and then frozen at -80C.

## (REQUIRED) nucleic acid extraction protocol: 
Gill tissue was disrupted using TissueLyser II (QIAGEN, Hilden, Germany). Total RNA from experiment 1 was extracted using the RNeasy Plus Universal Mini Kit (QIAGEN, Hilden, Germany). RNA concentrations and quality were measured using a NanoDrop ND2000c spectrophotometer (NanoDrop Technologies, Wilmington, DE, USA). RNA samples were kept frozen at -80 °C until further processing. 

## (REQUIRED) nucleic acid library construction protocol: 
TruSeq Stranded mRNA Sample Prep HS Protocol (Part # 15031057 Rev. E, October 2013). Used RAP (RNA Adapter Plate) 96-plex single unique indexes.

## (REQUIRED) nucleic acid sequencing protocol: 
Barcoded samples were sequenced in two pools on the Illumina HiSeq 2000.

## (OPTIONAL) growth protocol:

## (OPTIONAL) treatment protocol:

## (OPTIONAL) normalization data transformation protocol:

## (OPTIONAL) conversion protocol:

## (OPTIONAL) dissection protocol:

## (OPTIONAL) high throughput sequence alignment protocol:


# Sequencing library information

* Library Layout: SINGLE
* Library Source: TRANSCRIPTOMIC
* Library Strategy: RNA-Seq
* Library Selection: Oligo-dT
* Library Strand (Only for strand specific protocols): NA
* Nominal Length (Paired end only): NA
* Nominal SDev (Paired end only): NA
* Orientation (Paired end only): NA