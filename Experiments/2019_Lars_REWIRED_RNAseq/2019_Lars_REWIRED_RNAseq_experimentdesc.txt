# General information

## Title:
RNA-seq of 6 salmonids and 6 non-salmonid teleost fishes

## Description:
Evolution of gene expression after the salmonid-specific whole genome duplication (WGD). RNA-seq from 6 non-salmonid teleosts: Danio rerio (Drer), Oryzias latipes (Olat), Gasterosteus aculeatus (Gacu), Esox lucius (Eluc), Umbra pygmaea (Upyg), Novumbra hubbsi (Nhub), and 6 salmonids: Thymallus thymallus (Tthy), Oncorhynchus kisutch (Okis), Hucho hucho (Hhuc), Salmo salar (Ssal), Salvelinus alpinus (Salp), Oncorhynchus mykiss (Omyk). Four individuals, two male and two female, were sampled from each species. Four tissues (Li: Liver, Gi: gills, Br: Brain, Sp: Spleen) sampled from all twelve species. With additional heart (He) tissue from Salmo salar. To investigate splice variants, some tissues were sequenced deeper in Salmo salar (Liver, Brain, Sk: Skin, Mu: Muscle, Gon: Gonads) and Esox lucius (Liver, Brain, Muscle). The target depth for splice variant analysis were 100M reads/sample as opposed to 25M reads/sample for the rest of the samples. Samples that did not reach the target sequencing depth were re-sequenced (batch2).




## Contact info:
lars.gronvold@mmbu.no

# Protocols
(Write a description for each of the protocols)

## (REQUIRED) sample collection protocol:

## (REQUIRED) nucleic acid extraction protocol:

## (REQUIRED) nucleic acid library construction protocol:
cDNA library with Illumina TruSeq Stranded mRNA Sample Prep HS kit

## (REQUIRED) nucleic acid sequencing protocol:
PE150 (Paired-end 150bp)

## (OPTIONAL) growth protocol:

## (OPTIONAL) treatment protocol:

## (OPTIONAL) normalization data transformation protocol:

## (OPTIONAL) conversion protocol:

## (OPTIONAL) dissection protocol:

## (OPTIONAL) high throughput sequence alignment protocol:


# Sequencing library information

(These are chosen from a selection in a drop-down list in the Annotare app)

* Library Layout: Paired-end
* Library Source: the type of source material that is being sequenced. (e.g: TRANSCRIPTOMIC)
* Library Strategy: the sequencing technique intended for the library. (e.g: RNA-Seq)
* Library Selection: the method used to select and/or enrich the material being sequenced. (e.g. Oligo-dT)
* Library Strand (Only for strand specific protocols): whether the 'first strand' or 'second strand' of cDNA was used to prepare the library. 

* Nominal Length (Paired end only): the expected size of the insert.  
* Nominal SDev (Paired end only): the standard deviation of the nominal length. Decimals are allowed (e.g. 56.4) but no ranges (e.g. 34.5-42.6).
* Orientation (Paired end only): the orientation of the two reads. "5'-3'-3'-5'" for forward-reverse pairs, "5'-3'-5'-3'" for forward-forward pairs.
