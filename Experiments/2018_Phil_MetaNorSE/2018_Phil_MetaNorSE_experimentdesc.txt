# General information

## Title:
RNAseq of Atlantic salmon individuals hindgut
## Description:
Farmed salmon individuals from AquaGen AS was sampled and hindgut tissue was taken for RNAseq
## Contact info:
Simen R�d Sandve
simen.sandve@nmbu.no

# Protocols
(Write a description for each of the protocols)

## (REQUIRED) sample collection protocol:
All fish was killed with a sharp blow to the head. Hindgut was taken, flashed frozen in liquid nitrigen and stored in -80 before RNAseq. The fish had not been starved prior to sampling.

## (REQUIRED) nucleic acid extraction protocol:
Total RNA was extracted with Qiagen RNeasy Plus Universal Kit. RNA quality was measured with Nanodrop8000 and Agilent Bioanalyzer 2100. 

## (REQUIRED) nucleic acid library construction protocol:
Library preperation was done with TruSeq Stranded mRNA Library Prep Kit (Illumina, San Diego, CA, USA). 

## (REQUIRED) nucleic acid sequencing protocol:
Samples were sequenced using 100bp single-end high-throughput mRNA sequencing (RNA-seq) on Illumina Hiseq 4000 (Illumina, San Diego, CA, USA) in Norwegian Sequencing Centre (Oslo, Norway)
