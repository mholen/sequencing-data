## Sample metadata repository for sequencing experiments

This repository contains metadata about our RNA-seq/ATAC-seq experiments, including sample metadata, experiment description and the location of the raw data (i.e. fastq files).

### Folder and file organization

The files for each experiment should be located in its own folder under the __Experiments__ folder. Folders should be named like this: __Year_personresponsible_shortexperimentname__

In each experiment folder there should be atleast two files:

1. __Experiment description:__ Description of experiment based on the template __Templates/template_experimentdesc.txt__. The filename should be the same as the folder name and must end with ___experimentdesc.txt__
2. __Sample metadata:__ Describes the samples including paths to raw fastq data. Use the template __Templates/template_samplemeta.xlsx__. The filename should be the same as the folder name and must end with ___samplemeta.xlsx__


Notes on some sample metadata table columns:

* __Directory__:  The folder where the fastq files are stored on the server. The files must wither be in this folder or in a subdirectory. The fastq files must be gzip'ed and have names ending with __.fastq.gz__
* __Sample name__: Name of the sample. This name should to be part of the fastq filename. Since the sequencing centre does not allow underscores in the sample name, it is recommended to use dash sign "-" to separate experiment variables (e.g. R2-D2-1). 
* __Sample ID__: (Optional) If, for some reason you want to name your samples differently than they are in the fastq files, you should add this column, which should then contain the sample names used to recognize the fastq files.


### Submission to ArrayExpress

The experiment description and sample metadata table should correspond to the data you need to input when you submit to ArrayExpress through their Annotare interface. However, to make it easier to upload large number of files there are some scripts to help.

Open `runSubmissionScripts.R` in RStudio, enter the full path to the sample metadata table files and the FTP submission folder you got from Annotare and run through the script. This should generate three files:

1. __yourExperiment_sampleFiles.csv__: A table with fastq file names for each sample.
2. __yourExperiment_FTPupload.sh__: Script for uploading the files using FTP.
3. __yourExperiment_sampleFiles.md5__: md5 checksums for the files.

Once you have generated the FTP upload script, log on to the orion server in a terminal and submit the script to cluster like this:

`sbatch -w cn-11 path/to/my_experiment_FTPupload.sh`

(note: "-w cn-11" is needed because ftp is not installed on all nodes. TODO: use "lftp" instead. )

Once the upload is finished (could take some time), copy paste the md5 checksums into annotare. The Annotare will then verify the uploaded files (this takes some minutes). Once the files are verified, open the __yourExperiment_sampleFiles.csv__ table in a spreadsheet and copy the RawDataFileX columns one at the time into Annotore using the "Paste Into Column" button.