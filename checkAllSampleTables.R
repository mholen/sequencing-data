# Check samplemeta.xlsx files.
#
# Run through all the Experiment directories and check if they have the required files
# and that they point to the correct directory with the fastq files

library(tidyverse)


source("scripts/findFastqFiles.R")


XLsuffix <- "_samplemeta.xlsx"

for( expName in dir("Experiments",full.names = F)){
  cat("\nExperiment:",expName,"\n")
  
  inputXLfile <- dir(file.path("Experiments",expName),pattern = "_samplemeta.xlsx",
      full.names = T)
  if(length(inputXLfile)==0) {cat("Error: _samplemeta.xlsx not found!");next}
  if(length(inputXLfile)>1) {cat("Error: too many _samplemeta.xlsx files!");next}
  cat("Reading samplemeta file:",basename(inputXLfile),"\n")
  
  filePrefix = substring(inputXLfile, 1,nchar(inputXLfile)-nchar(XLsuffix))
  outputCSVfile <- paste0(filePrefix,"_samplefiles.csv")
  
  tryCatch(
    # need to use withCallingHandlers for warnings or else it will stop after the first warning
    withCallingHandlers(findFastqFiles(inputXLfile,outputCSVfile), warning = function(e) {cat("Warning:",e$message,"\n")}),
    error = function(e) {cat("Error:",e$message,"\n"); warning("Error: ",e$message,"  In experiment: ",expName)}
  ) 
}

